#!/usr/bin/bash

function print_armstrong( )
{
    
    i=`expr $1 \* 1`
    while [ $i -le $2 ]
    do
        sum=0
        k=100
        while [ $k / 10 -ne 0 ]
        do
            j=`expr $i % 10`
            sum=`expr $sum + $j`
            k=`expr $i / 10`
        done
        if [ $sum -eq $i ]
        then 
            echo "$i"
        fi
        i=`expr $i + 1`
    done
}

clear

while [ true ]
do
    echo "Enter your choice"
    echo "0.Exit"
    echo "1.Print all Armstrong numbers between given range"
    echo "2.Print Pattern"
    echo "3.Print addtition of two floating point numbers"
    echo "4.Add execute permission for user of file"
    echo "5.Convert Upper Case letters of given string to lower case"
    echo "6.Find file with name in your home directory"
    echo "7.Count number of occurrences of a word in file"
    read choice

    if [ $choice -eq 0 ]
    then 
        exit 
    fi 

    case $choice in 
    1)
        print_armstrong $1 $2
        ;;
    3)
        echo "Enter float num 1"
        read n1
        echo "Enter float num 2"
        read n2
        sum=`echo "$n1 + $n2" | bc`
        echo "addition = $sum"
        ;;    
    4)
        echo "Enter file name"
        read filename
        if [ -e filename ]
        then
            if [ -f filename ]
            then
                chmod +x filename
            fi
        else
            echo "File is either directory or invalid filename"
        fi
        ;;
    5)
        echo "Enter the string"
        read string
        echo $string | tr [:upper:] [:lower:]
        ;;
    6)
        echo "Enter file name"
        read file
        find /home -file
        ;;
    7)
        echo "Enter full name of file"
        read fullfilename
        echo "Enter word"
        read key
        count=0
        for word in `cat fullfilename`
        do  
            if [ $word -eq $key ]
            then
                count=`expr $count + 1`
            fi
        done
        echo "$key occured $count times in the given file"
        ;;
    esac
done

exit



